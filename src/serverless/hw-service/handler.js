'use strict';


module.exports.hello = async (event, context) => {
  
  return {
    "isBase64Encoded": false,
    "statusCode": 200,
    "headers": { "Access-Control-Allow-Origin": "*"},
    "body":  "Hello from my lambda aws hello world function"
  }
}