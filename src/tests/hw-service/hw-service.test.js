const axios = require('axios');
const httpAdapter =require( 'axios/lib/adapters/http')
axios.defaults.adapter = httpAdapter;

test("Test hw-service", async()=>{
    let resp = await axios.get('https://smqv52ls30.execute-api.us-west-1.amazonaws.com/dev/helloworld');
    expect(resp).toBe("Hello world message lambda from serverless, Kai Cooper. With updated lambda");
})

